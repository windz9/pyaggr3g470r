#!/bin/bash

# Installation of MongoDB
sudo apt-get install -y mongodb-server
sudo service mongodb start

# Python dependencies
sudo apt-get install -y python-pip
pip install --user virtualenv
virtualenv --no-site-packages ./env_pyAggr3g470r
source ./env_pyAggr3g470r/bin/activate
pip install --upgrade -r requirements.txt

# Configuration
cp conf/conf.cfg-sample conf/conf.cfg
python pyaggr3g470r/initialization.py firstname lastname firstname.lastname@mail.com secret

# Launch pyAggr3g470r
python runserver.py

deactivate
